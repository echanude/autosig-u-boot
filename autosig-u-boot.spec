Name:           autosig-u-boot
Version:        2022.07
Release:        1%{?dist}
Summary:        U-boot with android boot support for qemu

License:        GPL-2.0+

Source0:        https://source.denx.de/u-boot/u-boot/-/archive/v2022.07/u-boot-v2022.07.tar.gz
# Support "part type" command to do per-uuid type checks
# Upstreamed: https://patchwork.ozlabs.org/project/uboot/patch/20220727133534.126824-1-eballetbo@redhat.com/
Patch1:         u-boot-get-partition-type-guid.patch
# Tweak config to auto-boot aboot partitions
Patch2:         u-boot-auto-aboot.patch

BuildRequires:  gcc make swig bison flex python3-devel openssl-devel python3-setuptools

%define debug_package %{nil}

%ifarch aarch64
%define uboot_arch arm64
%define uboot_config qemu_%{uboot_arch}_defconfig
%else
%define uboot_arch %{_arch}
%define uboot_config qemu-%{uboot_arch}_defconfig
%endif

%description
This is a build of u-boot for the purpose of using with qemu to
demonstrate booting using android boot partitions (aboot).
Do not use in physical systems.

%prep
%setup -q -n u-boot-v%{version}
%patch1 -p1
%patch2 -p1

echo 'CONFIG_ANDROID_BOOT_IMAGE=y' >> configs/%{uboot_config}
echo 'CONFIG_CMD_ABOOTIMG=y' >> configs/%{uboot_config}
echo 'CONFIG_CMD_ADTIMG=y' >> configs/%{uboot_config}
echo 'CONFIG_ANDROID_AB=y' >> configs/%{uboot_config}
echo 'CONFIG_CMD_AB_SELECT=y' >> configs/%{uboot_config}
echo 'CONFIG_CMD_BCB=y' >> configs/%{uboot_config}
echo 'CONFIG_CMD_GPT=y' >> configs/%{uboot_config}
echo 'CONFIG_PARTITION_TYPE_GUID=y' >> configs/%{uboot_config}

%build
make %{uboot_config}
make -j$(nproc)

%install
mkdir -p $RPM_BUILD_ROOT/boot

%ifarch x86_64
# On x86 the .rom file is used
cp u-boot.rom $RPM_BUILD_ROOT/boot/u-boot.bin
%else
cp u-boot.bin $RPM_BUILD_ROOT/boot/u-boot.bin
%endif

%files
/boot/u-boot.bin

%changelog
* Fri Aug 26 2022 Alexander Larsson <alexl@redhat.com> - 2022.07-1
- Initial version with added aboot patches
